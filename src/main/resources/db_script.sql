CREATE DATABASE IF NOT EXISTS osbb_db;
USE osbb_db;

DROP TABLE IF EXISTS apartment;
DROP TABLE IF EXISTS building;

CREATE TABLE building
(
	id INTEGER AUTO_INCREMENT,
    city_id INTEGER NOT NULL,
    street_id INTEGER NOT NULL,
    building_address VARCHAR(5),
    apartment_amount INTEGER NOT NULL,
    building_overall_square DECIMAL(10,2),
    PRIMARY KEY (id)
);
CREATE TABLE apartment
(
	id INTEGER AUTO_INCREMENT,
    building_id INTEGER NOT NULL,
    square DECIMAL(10,2) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (building_id) REFERENCES building (id) ON DELETE CASCADE ON UPDATE CASCADE
)